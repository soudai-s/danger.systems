- title: "Enforce changelogs"
  id: "changelogs"
  repos: "danger/danger-js"
  code: |
    // Add a CHANGELOG entry for app changes
    const hasChangelog = includes(danger.git.modified_files, "changelog.md")
    const isTrivial = contains((danger.github.pr.body + danger.github.pr.title), "#trivial")

    if (!hasChangelog && !isTrivial) {
      warn("Please add a changelog entry for your changes.")
    }

- title: "Keep Lockfile up to date"
  id: "prose"
  repos: "danger/danger-js"
  code: |
    const packageChanged = includes(danger.git.modified_files, 'package.json');
    const lockfileChanged = includes(danger.git.modified_files, 'yarn.lock');
    if (packageChanged && !lockfileChanged) {
      const message = 'Changes were made to package.json, but not to yarn.lock';
      const idea = 'Perhaps you need to run `yarn install`?';
      warn(`${message} - <i>${idea}</i>`);
    }

- title: "Encourage smaller PRs"
  id: "merge"
  repos: "ReactiveX/rxjs"
  code: |
    var bigPRThreshold = 600;
    if (danger.github.pr.additions + danger.github.pr.deletions > bigPRThreshold) {
      warn(':exclamation: Big PR (' + ++errorCount + ')');
      markdown('> (' + errorCount + ') : Pull Request size seems relatively large. If Pull Request contains multiple changes, split each into separate PR will helps faster, easier review.');
    }

- title: "Ensure PRs have assignees"
  id: "focus"
  repos: "artsy/emission"
  code: |
    // Always ensure we assign someone, so that our Slackbot can do its work correctly
    if (pr.assignee === null) {
      fail("Please assign someone to merge this PR, and optionally include people who should review.")
    }

- title: "Encourage more testing"
  id: "special"
  repos: "artsy/emission"
  code: |
    // Check that every file touched has a corresponding test file
    const correspondingTestsForAppFiles = touchedAppOnlyFiles.map(f => {
      const newPath = path.dirname(f)
      const name = path.basename(f).replace(".ts", "-tests.ts")
      return `${newPath}/__tests__/${name}`
    })

    // New app files should get new test files
    // Allow warning instead of failing if you say "Skip New Tests" inside the body, make it explicit.
    const testFilesThatDontExist = correspondingTestsForAppFiles.filter(f => fs.existsSync(f))
    if (testFilesThatDontExist.length > 0) {
      const callout = acceptedNoTests ? warn : fail
      const output = `Missing Test Files:
        ${testFilesThatDontExist.map(f => `  - [] \`${f}\``).join("\n")}
        If these files are supposed to not exist, please update your PR body to include "Skip New Tests".
      `
      callout(output)
    }
